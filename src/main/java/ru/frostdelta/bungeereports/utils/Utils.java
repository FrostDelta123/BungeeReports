package ru.frostdelta.bungeereports.utils;

public class Utils {

    //Потом будет enum, наверное. Или нет
    public static String SEND_MOD_MESSAGE;// = config.getString("send-mod-message").replaceAll("'","");
    public static String DUMP_NOT_FOUND;// = config.getString("dump-not-found").replaceAll("'","");
    public static String DUMP_CREATED;// = config.getString("dump-created").replaceAll("'","");
    public static String DUMP_COMMAND_ERROR;// = config.getString("dump-command-error").replaceAll("'","");
    public static String CONFIG_RELOADED;// = config.getString("config-reload").replaceAll("'","");
    public static String SCREEN_CMMAND_ERROR;// = config.getString("screen-command-error").replaceAll("'","");
    public static String PLAYER_NOT_FOUND;// = config.getString("player-not-found").replaceAll("'","");
    public static String SPECTATE_ERROR;// = config.getString("spectate-command-error").replaceAll("'","");
    public static String REJECT;// = config.getString("reject").replaceAll("'","");
    public static String PUNISH_TIME;// = config.getString("ban-time").replaceAll("'","");
    public static String PUNISH_TYPE;// = config.getString("ban-type").replaceAll("'","");
    public static String REPORT_SENDER;// = config.getString("report-sender").replaceAll("'","");
    public static String REPORT_REASON;// = config.getString("report-reason").replaceAll("'","");
    public static String REPORT_COMMENT;// = config.getString("report-comment").replaceAll("'","");
    public static String NO_REPORTS;// = config.getString("no-reports").replaceAll("'","");
    public static String GET_REPORTS_INV_NAME;// = config.getString("getreports-inv-name").replaceAll("'","");
    public static String PUNISH_INV_NAME;// = config.getString("punish-inv-name").replaceAll("'","");
    public static String REASONS_INV_NAME;// = config.getString("reasons-inv-name").replaceAll("'","");
    public static String REPORTS_INV_NAME;// = config.getString("reports-inv-name").replaceAll("'","");
    public static String ACCEPT;// = config.getString("accept").replaceAll("'","");
    public static String SPECTATE;// = config.getString("spectate").replaceAll("'","");
    public static String REWARD_MESSAGE;// = config.getString("reward-message").replaceAll("'","");
    public static String SPECTATE_TOGGLE_OFF;// = config.getString("spectate-toggle-off").replaceAll("'","");
    public static String SPECTATE_PLAYER;// = config.getString("spectate-player").replaceAll("'","");
    public static String REPORT_ACCEPT;// = config.getString("report-accept").replaceAll("'","");
    public static String REPORT_REJECT;// = config.getString("report-reject").replaceAll("'","");
    public static String BAN_MESSAGE;// = config.getString("ban-message").replaceAll("'","");
    public static String MUTE_MESSAGE;// = config.getString("mute-message").replaceAll("'","");
    public static String CHAT_COMMENT;// = config.getString("chat-comment").replaceAll("'","");
    public static String SUCCESS_REPORT;// = config.getString("success-report").replaceAll("'","");

}
